n = 20
nums = range(1, n)
i = n
while True:
    if all(map(lambda x: not i % x, nums)):
        print i
        break
    i += n