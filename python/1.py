#1
# sum = 0
# for i in range(10000000):
#     sum += (lambda i: i if not i % 3 or not i % 5 else 0)(i)
# print sum

print reduce(lambda x, y: x + y, [(lambda i: i if not i % 3 or not i % 5 else 0)(i) for i in range(10000000)])