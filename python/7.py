import math
lst = []

n = 0
i = 1
while n < 10001:
    i += 1
    for j in lst:
        if j > int(math.sqrt(i) + 1):
            lst.append(i)
            n += 1
            break
        if not i % j:
            break
    else:
        lst.append(i)
        n += 1

print "%dth=%d" % (n, i)