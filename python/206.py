#Find the unique positive integer whose
# square has the form 1_2_3_4_5_6_7_8_9_0,
# where each "_" is a single digit
x = 1000000030

while x < 2e9:
    x += 40
    s = str(x * x)
    if s[::2] == "1234567890":
        break
    x += 60
    s = str(x * x)
    if s[::2] == "1234567890":
        break
print x
print x * x
