lst = []
i = 1
while i < 2000000:
    i += 1
    if (i > 10) and ((i % 10 == 5) or (i % 2 == 0)):
        continue

    for j in lst:
        if j*j - 1 > i:
            lst.append(i)
            break
        if not i % j:
            break
    else:
        lst.append(i)


print sum(lst)